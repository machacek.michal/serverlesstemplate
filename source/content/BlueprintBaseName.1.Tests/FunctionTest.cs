using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;
using Amazon.Lambda.APIGatewayEvents;

using BlueprintBaseName._1;

namespace BlueprintBaseName._1.Tests
{
    public class FunctionTest
    {
        public FunctionTest()
        {
        }

        [Fact]
        public void TetGetMethod()
        {
            var context = new TestLambdaContext();
            var request = new APIGatewayProxyRequest();

            Functions functions = new Functions();
            
            var response = functions.Get(request, context);
            Assert.NotNull(response);
        }
    }
}
