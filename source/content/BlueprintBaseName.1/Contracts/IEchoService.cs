using System.Threading.Tasks;

namespace BlueprintBaseName._1
{
    public interface IEchoService
    {
        Task<string> Echo();
    }
}
