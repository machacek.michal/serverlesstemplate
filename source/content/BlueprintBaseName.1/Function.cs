using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.Lambda.Core;
using Microsoft.Extensions.DependencyInjection;
using Temperature.ImportMetadata.Infrastructure.DB;
using Temperature.ImportMetadata.Infrastructure.Options;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace BlueprintBaseName._1
{
    public class Functions : LambdaBase
    {
        public Functions()
        {

        }

        protected override void ConfigureServices(IServiceCollection services)
        {
            services.AddAWSService<IAmazonDynamoDB>();
            // services.AddTransient<IRepository<TemperatureDay>, Repository<TemperatureDay>>();
            // services.AddTransient<ITableRepository<TemperatureDayEntity>, TableRepository<TemperatureDayEntity>>();
            // services.AddTransient<IEchoService, EchoService>();
        }

        protected override void InjectOptions(IServiceCollection services)
        {
            services.Configure<BasicServiceOptions>(Configuration.GetSection("BasicServiceOptions"));
            services.Configure<DynamoDbOptions>(Configuration.GetSection("DynamoDbOptions"));
        }

        public async Task<string> Get(object request, ILambdaContext context)
        {
            var service = this.GetService<IEchoService>();
            return await service.Echo();
        }
    }
}
