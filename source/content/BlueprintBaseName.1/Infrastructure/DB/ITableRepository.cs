using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlueprintBaseName._1.Infrastructure.DB
{
    public interface ITableRepository<TEntity> where TEntity : class
    {
        Task Update(string tableName, TEntity entity);
        Task BatchUpdate(string tableName, List<TEntity> entities);
        Task<TEntity> Get(string tableName, object hashKey, object rangeKey);
    }
}
