using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlueprintBaseName._1.Infrastructure.Options;

namespace BlueprintBaseName._1.Infrastructure.DB
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly IDynamoDBContext dbContext;
        private readonly ILogger logger;
        private readonly DynamoDbOptions dbOptions;

        public Repository(IAmazonDynamoDB client, ILogger<Repository<TEntity>> logger, IOptions<DynamoDbOptions> dbOptions)
        {
            this.logger = logger;
            this.dbOptions = dbOptions.Value;

            this.dbContext = new DynamoDBContext(client, new DynamoDBOperationConfig
            {
                //TODO TableNamePrefix = this.dbOptions.Prefix for envirnonment table
            });
        }

        public async Task Save(TEntity entity)
        {
            await this.dbContext.SaveAsync(entity);
        }

        public async Task Delete(TEntity entity)
        {
            await this.dbContext.DeleteAsync(entity);
        }

        public async Task Delete(object hashKey, object rangeKey)
        {
            await this.dbContext.DeleteAsync<TEntity>(hashKey, rangeKey);
        }

        public async Task<TEntity> Get(object hashKey, object rangeKey)
        {
            return await this.dbContext.LoadAsync<TEntity>(hashKey, rangeKey);
        }

        public async Task<IEnumerable<TEntity>> Query(object hashKey)
        {
            var asyncSearch = this.dbContext.QueryAsync<TEntity>(hashKey);
            return await asyncSearch.GetRemainingAsync();
        }

        public async Task<IEnumerable<TEntity>> Scan(List<ScanCondition> scanConditions)
        {
            var asyncSearch = this.dbContext.ScanAsync<TEntity>(scanConditions);
            return await asyncSearch.GetRemainingAsync();
        }
    }
}
