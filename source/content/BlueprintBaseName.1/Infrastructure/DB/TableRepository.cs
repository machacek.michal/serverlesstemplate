using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlueprintBaseName._1.Infrastructure.Options;

namespace BlueprintBaseName._1.Infrastructure.DB
{
    public class TableRepository<TEntity> : ITableRepository<TEntity> where TEntity : class
    {
        private readonly IAmazonDynamoDB dbClient;
        private readonly DynamoDbOptions dbOptions;

        public TableRepository(IAmazonDynamoDB dbClient,
                              IOptions<DynamoDbOptions> dbOptions)
        {
            this.dbClient = dbClient;
            this.dbOptions = dbOptions.Value;
        }

        private Table GetTable(string tableName)
        {
            var tableFullName = $"{dbOptions.Prefix}-{tableName}";
            return Table.LoadTable(dbClient, tableFullName);
        }

        public async Task Update(string tableName, TEntity entity)
        {
            var table = GetTable(tableName);
            var json = JsonConvert.SerializeObject(entity);
            var document = Document.FromJson(json);
            //var hashKey = new Primitive(hashKeySelector(entity).ToString());
            await table.UpdateItemAsync(document);
        }

        public async Task BatchUpdate(string tableName, List<TEntity> entities)
        {
            var table = GetTable(tableName);

            var batch = table.CreateBatchWrite();

            entities.ForEach(e =>
            {
                var json = JsonConvert.SerializeObject(e);
                var document = Document.FromJson(json);
                batch.AddDocumentToPut(document);
            });

            await batch.ExecuteAsync();
        }

        public async Task<TEntity> Get(string tableName, object hashKey, object rangeKey)
        {
            var table = GetTable(tableName);
            var document = await table.GetItemAsync(new Primitive(hashKey.ToString()));
            var json = document?.ToJson() ?? string.Empty;
            return JsonConvert.DeserializeObject<TEntity>(json);
        }
    }
}
