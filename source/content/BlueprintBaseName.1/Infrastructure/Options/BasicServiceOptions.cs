using System.Collections.Generic;

namespace BlueprintBaseName._1.Infrastructure.Options
{
    public class BasicServiceOptions
    {
        public Dictionary<string, string> IntervalTables { get; set; }
    }
}
