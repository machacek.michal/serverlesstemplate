using AutoMapper;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace BlueprintBaseName._1.Services
{
    public class EchoService : IEchoService
    {
        //private readonly ITableRepository<TemperatureDayEntity> dataRepository;
        //private readonly BasicServiceOptions serviceOptions;
        //private readonly IMapper mapper;

        public EchoService(/*ITableRepository<TemperatureDayEntity> dataRepository,
                                      IOptions<BasicServiceOptions> serviceOptions,
                                      IMapper mapper*/)
        {
            //this.dataRepository = dataRepository ?? throw new ArgumentNullException(nameof(dataRepository));
            //this.serviceOptions = serviceOptions.Value ?? throw new ArgumentNullException(nameof(serviceOptions));
            //this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<string> Echo()
        {
            return await Task.FromResult("Hello world!");
        }
       
    }
}
